# AZRS

**Repozitorijum posvećen projektu na kursu Alati za razvoj softvera, Matematički fakultet u Beogradu**

Svi alati biće primenjeni na projektu Data Mining Toolbox u toku njegovog daljeg razvoja. Planovi za popravke i nove funkcionalnosti pomenutog projekta se takođe mogu pratiti na tabli.

Data Mining Toolbox je grupni projekat razvijen na kursu Razvoj softvera. Link do repozitorijuma: https://github.com/backspacer303/RS012-data-mining-toolbox

### Alati:
- git flow
- GDB
- CI
- git hook
- CMake
- Valgrind (callgrind, massif)
- Gammaray
- ClangTidy
- ClangFormat
- Clang Static Analyzer (scan-build, CodeChecker)
- Docker
- GCov
